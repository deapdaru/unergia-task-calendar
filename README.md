# Calendar Task App

![Herko](https://heroku-badge.herokuapp.com/?app=calendar-task-app)

Site live at - https://calendar-task-app.herokuapp.com/

## Technologies used

|          | Technologies                                                                                                                    |
|----------|---------------------------------------------------------------------------------------------------------------------------------|
| **Frontend** | <ul><li>React<ul><li>create-react-app</li><li>react-draggable</li></ul></li><li>Material-UI<ul><li>@material-ui/core</li><li>@material-ui/icons</li><li>@material-ui/lab</li><li>@material-ui/pickers</li></ul></li><li>FullCalendar<ul><li>@fullcalendar/react</li><li>@fullcalendar/daygrid</li><li>@fullcalendar/interaction</li></ul></li></ul> |
| **Backend**  | <ul><li>Node.js<ul><li>dotenv</li><li>concurrently</li><li>morgan</li></ul></li><li>Express<ul><li>express-generator</li></ul></li><li>MongoDB (Mongoose ODM)<ul><li>mongoose</li></ul></li></ul> |
| **Other**    | <ul><li>npm</li><li>Git</li></ul> |

***

## Deployment

- Express and React app through *Heroku*
- MongoDB database through *MongoDB Atlas*

***

## Screenshots of features

- ### Calendar Month view
  ![Calendar](images/calendar.png "Calendar")

- ### Adding a new task
  Will by default have end date as the next date of the selected date, although end date can be changed. <br/>
  Can be done in two ways:
  - Click on the date cell on calendar to add a day long task for that day.
  - Click on 'Add Task' button to add a new task for today.

  ![Adding Task](images/addTask.png "Adding Task")

- ### Confirmation alerts
  Alert Snackbars appear when:
  - Task is added
  - Task is edited
  - Task is deleted

  ![Task Add Alert](images/alertAdd.png "Task Add Alert")
  ![Task Delete Alert](images/alertDelete.png "Task Delete Alert")

- ### Editing existing task
  To do so click on the task <br />
  Task status can be changed here <br />
  Different colours for the status:
  - **To-Do** - grey
  - **In-Progress** - blue
  - **Completed** - green

  ![Editing Task](images/editTask.png "Editing Task")

- ### Draggable components
  - #### Tasks
    Not only are tasks draggable but also resizable at both ends.
    ![Draggable Task](images/draggableTask.png "Draggable Task")

  - #### Add Task and Edit Task Modals
    ![Draggable Modal](images/draggableModal.png "Draggable Modal")

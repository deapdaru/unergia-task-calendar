var createError = require('http-errors');
var express = require('express');
var logger = require('morgan');
const path = require('path');
var mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config();

var tasksRouter = require('./routes/tasks');

const url = process.env.MONGODB_URI;
const connect = mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });

connect.then((db) => {
  console.log("Connected correctly to server");
}, (err) => { console.log(err); });

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');

  next();
}

app.use(allowCrossDomain)
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

if(process.env.NODE_ENV === 'production') {
  app.use(express.static('frontend/build'));  
  app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname,'frontend/build/index.html'));
  })
} else {
  app.use(express.static(path.join(__dirname, 'frontend/public')));
}

app.use('/tasks', tasksRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

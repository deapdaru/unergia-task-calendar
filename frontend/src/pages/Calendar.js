import React, { useEffect, useState } from 'react';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import bootstrapPlugin from '@fullcalendar/bootstrap';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.css';
import '@fortawesome/fontawesome-free/css/all.css';
import { 
  // Checkbox, 
  Dialog, 
  // DialogActions, 
  DialogContent, 
  DialogTitle, 
  // FormControlLabel, 
  // FormGroup, 
  Grid, 
  IconButton, 
  TextField, 
  Button,
  Paper,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  Snackbar,
  Tooltip
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import Draggable from 'react-draggable';

import 'moment';
import MomentUtils from '@date-io/moment';
import {
  MuiPickersUtilsProvider,
  // KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { grey, blue, green } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: '600px',
    overflowX: 'auto'
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  form: {
    margin: theme.spacing(2)
  },
  button: {
    margin: theme.spacing(2),
    width: "100%"
  },
  grey: {
    backgroundColor: grey[500],
    '&:hover': {
      backgroundColor: grey[300]
    }
  },
  blue: {
    backgroundColor: blue[500],
    '&:hover': {
      backgroundColor: blue[300]
    },
  },
  green: {
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[300]
    },
  }
}));

function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

function Calendar() {
  var today = new Date();
  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate()+1);

  const [tasks, setTasks] = useState([]);
  const [openAddEvent, setOpenAddEvent] = useState(false);
  const [addTask, setAddTask] = useState({
    title: "",
    allDay: true,
    start: today.toLocaleDateString(),
    end: tomorrow.toLocaleDateString()
  });
  const [openEditEvent, setOpenEditEvent] = useState(false);
  const [editTask, setEditTask] = useState({
    id: "",
    title: "",
    allDay: true,
    start: "",
    end: "",
    status: ""
  });

  const [dateError, setDateError] = useState(false);

  const [successAddTask, setSuccessAddTask] = useState(false);
  const [successEditTask, setSuccessEditTask] = useState(false);
  const [successEditTaskStatus, setSuccessEditTaskStatus] = useState(false);
  const [successDeleteTask, setSuccessDeleteTask] = useState(false);

  const classes = useStyles();

  // add event dialog close function
  const handleAddEventClose = () => {
    setOpenAddEvent(false);
  };

  // add event dialog close function
  const handleEditEventClose = () => {
    setOpenEditEvent(false);
  };

  // handle value changes for new task for any date
  const addTaskHandle = (event) => {
    setAddTask({...addTask, [event.target.name]: event.target.value});
  };

  const editTaskHandle = (event) => {
    setEditTask({...editTask, [event.target.name]: event.target.value});
  }

  // create new task for any date
  const addTaskSubmit = async () => {
    var config = {
      method: 'post',
      url: '/tasks',
      headers: { 
        'Content-Type': 'application/json'
      },
      data: addTask
    };
    
    try {
      var request = await axios(config)
      setSuccessAddTask(true);
      setTimeout(() => {
        setSuccessAddTask(false);
      }, 3000);
      var obj = {
        id: request.data._id,
        start: request.data.start,
        end: request.data.end,
        allDay: request.data.allDay,
        title: request.data.title,
        status: request.data.status,
        backgroundColor: request.data.status === 'To-Do' ? grey[500] : request.data.status === 'In-Progress' ? blue[500] : green[500]
      }
      setTasks([...tasks, obj])
      setOpenAddEvent(false)
      setAddTask({...addTask, title: ""})
    } catch (err) {
      console.log(err);
    }
  }

  // edit task for any date
  const editTaskSubmit = async () => {
    var config = {
      method: 'put',
      url: '/tasks/' + editTask.id,
      headers: {
        'Content-Type': 'application/json'
      },
      data : editTask
    };
    
    try {
      var request = await axios(config);
      setSuccessEditTask(true);
      setTimeout(() => {
        setSuccessEditTask(false)
      }, 3000);
      var clone = [...tasks];
      clone.map((task) => {
        if (task.id === request.data._id) {
          task.id = request.data._id;
          task.start = request.data.start;
          task.end = request.data.end;
          task.allDay = request.data.allDay;
          task.title = request.data.title;
          task.status = request.data.status;
          task.backgroundColor = request.data.status === 'To-Do' ? grey[500] : request.data.status === 'In-Progress' ? blue[500] : green[500];
        }
        return 0;
      })
      setTasks(clone);
      setOpenEditEvent(false);
    } catch (err) {
      console.log(err);
    }
  }

  const nextStatusEdit = async () => {
    var config = {
      method: 'put',
      url: '/tasks/' + editTask.id,
      headers: {
        'Content-Type': 'application/json'
      },
      data : {
        status: editTask.status === 'To-Do' ? 'In-Progress' : 'Completed'
      }
    };
    
    try {
      var request = await axios(config);
      setSuccessEditTaskStatus(true);
      setTimeout(() => {
        setSuccessEditTaskStatus(false)
      }, 3000);
      var clone = [...tasks];
      clone.map((task) => {
        if (task.id === request.data._id) {
          task.status = request.data.status;
          task.backgroundColor = request.data.status === 'To-Do' ? grey[500] : request.data.status === 'In-Progress' ? blue[500] : green[500];
        }
        return 0;
      })
      setTasks(clone);
      setOpenEditEvent(false);
    } catch (err) {
      console.log(err);
    }
  }

  // delete an existing task
  const deleteTask = async () => {
    var config = {
      method: 'delete',
      url: '/tasks/' + editTask.id,
      headers: {
        'Content-Type': 'application/json'
      }
    };
    
    try {
      var request = await axios(config);
      setSuccessDeleteTask(true);
      setTimeout(() => {
        setSuccessDeleteTask(false);
      }, 3000);
      var clone = tasks.filter((task) => task.id !== request.data._id);
      setTasks(clone);
      setOpenEditEvent(false);
    } catch (err) {
      console.log(err);
    }
  }

  // loads initial tasks
  async function loadTasks() {
    try {
      var config = {
        method: 'get',
        url: '/tasks',
        headers: {
          'Content-Type': 'application/json',
        },
      };
      
      var request = await axios(config);
      var taskList = [];
      request.data.forEach(task => {
        var obj = {
          id: task._id,
          allDay: task.allDay,
          start: task.start,
          end: task.end,
          title: task.title,
          status: task.status,
          backgroundColor: task.status === 'To-Do' ? grey[500] : task.status === 'In-Progress' ? blue[500] : green[500]
        };
        taskList.push(obj);
      });
      setTasks(taskList);
    }
    catch (err) {
      console.log(err);
    }
  }

  // handles the drag and drop as well as resize change of tasks
  function handleTaskChange(info) {
    var clone = [...tasks];
    clone.map(task => {
      if(task.id === info.event.id) {
        var data = JSON.stringify({
          "start": info.event.start,
          "end": info.event.end
        });
        
        var config = {
          method: 'put',
          url: '/tasks/' + info.event.id,
          headers: { 
            'Content-Type': 'application/json'
          },
          data : data
        };
        try {
          axios(config)
          task.start = info.event.start;
          task.end = info.event.end;
        } catch (err) {
          console.log(err);
        }
      }
      return 0;
    })
    setTasks(clone);
  }

  function renderEventContent(eventInfo) {
    console.log(eventInfo);
    return (
      <div style={{display:'flex', justifyContent: 'center'}}>
        <div>
          {eventInfo.event.title}
        </div>
      </div>
    )
  }

  useEffect(() => {
    loadTasks();
  }, [])

  return (
    <div className={classes.root}>
      <FullCalendar
        plugins={[ interactionPlugin, dayGridPlugin, bootstrapPlugin ]}
        eventContent={renderEventContent}
        initialView="dayGridWeek"
        height='100vh'
        windowResizeDelay={0}
        stickyHeaderDates={true}
        headerToolbar={{
          start:'prevYear,prev,next,nextYear today addTaskButton',
          center: 'title',
          end: 'dayGridWeek,dayGridMonth'
        }}
        customButtons={{
          addTaskButton: {
            text: 'ADD TASK',
            click: () => {
              setOpenAddEvent(true)
              setAddTask({ ...addTask, start: today, end: tomorrow })
            }
          }
        }}
        buttonText={{
          today: 'JUMP TO TODAY'
        }}
        themeSystem='bootstrap'
        views={{
          dayGridMonth: {
            dayHeaderFormat: {
              weekday: 'long',
            }
          },
          dayGridWeek: {
            dayHeaderFormat: {
              weekday: 'long',
              day: 'numeric'
            },
            titleFormat: {
              month: 'long',
              year: 'numeric'
            }
          }
        }}
        weekNumbers={true}
        weekNumberFormat={{
          week: 'numeric'
        }}
        selectable={true}
        dateClick={(info) => {
          setOpenAddEvent(true)
          setAddTask({...addTask, start: info.date, end: new Date(info.date).setDate(info.date.getDate()+1)})
        }}
        events={tasks}
        editable
        eventResizableFromStart
        dayMaxEventRows={4}
        eventClick={(eventClickInfo) => {
          var statusInfo;
          tasks.map((task) => {
            if(task.id === eventClickInfo.event.id){
              statusInfo = task.status
            }
            return 0;
          })
          setOpenEditEvent(true)
          setEditTask({
            id: eventClickInfo.event.id,
            title: eventClickInfo.event.title,
            allDay: eventClickInfo.event.allDay,
            start: eventClickInfo.event.start,
            end: eventClickInfo.event.end,
            status: statusInfo
          })
        }}
        eventChange={(eventChangeInfo) => {
          handleTaskChange(eventChangeInfo);
        }}
      />

      <Dialog PaperComponent={PaperComponent} fullWidth maxWidth={'md'} open={openAddEvent} onClose={handleAddEventClose}>
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          Add Task
          <IconButton className={classes.closeButton} onTouchEnd={() => setTimeout(() => {
            handleAddEventClose()
          }, 600)} onClick={handleAddEventClose}>
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent className={classes.root} dividers>
          <Grid container spacing={2}>
            <Grid item xs={false} md={2}></Grid>
            <Grid item xs={12} md={8}>
              <TextField
                className={classes.form}
                name="title"
                autoFocus
                fullWidth
                label="Title"
                value={addTask.title}
                error={addTask.title === ""}
                helperText={addTask.title === "" ? "Required" : ""}
                onChange={addTaskHandle}
              />
            </Grid>
            <Grid item xs={false} md={2}></Grid>

            <Grid item xs={false} md={2}></Grid>
            <Grid item xs={12} md={4}>
              <MuiPickersUtilsProvider utils={MomentUtils}>
                <KeyboardDatePicker
                  className={classes.form}
                  margin="normal"
                  name="start"
                  fullWidth
                  label="Start Date"
                  format="DD/MM/yyyy"
                  value={addTask.start}
                  error={dateError || addTask.start >= addTask.end}
                  helperText={dateError || addTask.start >= addTask.end ? "Enter valid date!" : ""}
                  onChange={(date) => {
                    if(date._isValid) {
                      setAddTask({...addTask, start: date})
                      setDateError(false)
                    }
                    else
                      setDateError(true)
                  }}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item xs={12} md={4}>
              <MuiPickersUtilsProvider utils={MomentUtils}>
                <KeyboardDatePicker
                  className={classes.form}
                  margin="normal"
                  name="end"
                  fullWidth
                  label="End Date"
                  format="DD/MM/yyyy"
                  value={addTask.end}
                  error={dateError || addTask.start >= addTask.end}
                  helperText={dateError || addTask.start >= addTask.end ? "Enter valid date!" : ""}
                  onChange={(date) => {
                    if(date._isValid) {
                      setAddTask({...addTask, end: date})
                      setDateError(false)
                    }
                    else
                      setDateError(true)
                  }}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item xs={false} md={2}></Grid>

            {/* <Grid item xs={0} md={2}></Grid>
            <Grid item xs={12} md={10}>
              <FormGroup row>
                <FormControlLabel
                  control={<Checkbox checked={addTask.allDay} name="allDay" onChange={() => setAddTask({...addTask, allDay: !addTask.allDay})} />}
                  label="All Day Task"
                />
              </FormGroup>
            </Grid> */}

            <Grid item xs={false} md={6}></Grid>
            <Grid item xs={12} md={4}>
              <Button
                className={classes.button}
                onClick={addTaskSubmit}
                color="primary"
                variant="contained"
                disabled={addTask.title === "" || dateError || addTask.start >= addTask.end}
              >
                ADD
              </Button>
            </Grid>
            <Grid item xs={false} md={2}></Grid>
            
          </Grid>
        </DialogContent>
      </Dialog>

      <Dialog PaperComponent={PaperComponent} fullWidth maxWidth={'md'} open={openEditEvent} onClose={handleEditEventClose}>
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          Edit Task
          <IconButton className={classes.closeButton} onTouchEnd={() => setTimeout(() => {
            handleEditEventClose()
          }, 600)} onClick={handleEditEventClose}>
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <Grid container spacing={1}>
            <Grid item xs={false} md={2}></Grid>
            <Grid item xs={12} md={8}>
              <TextField
                className={classes.form}
                name="title"
                autoFocus
                fullWidth
                label="Title"
                value={editTask.title}
                error={editTask.title === ""}
                helperText={editTask.title === "" ? "Required" : ""}
                onChange={editTaskHandle}
              />
            </Grid>
            <Grid item xs={false} md={2}></Grid>

            <Grid item xs={false} md={2}></Grid>
            <Grid item xs={12} md={4}>
              <MuiPickersUtilsProvider utils={MomentUtils}>
                <KeyboardDatePicker
                  className={classes.form}
                  margin="normal"
                  name="start"
                  fullWidth
                  label="Start Date"
                  format="DD/MM/yyyy"
                  value={editTask.start}
                  error={dateError || editTask.start >= editTask.end}
                  helperText={dateError || editTask.start >= editTask.end ? "Enter valid date!" : ""}
                  onChange={(date) => {
                    if(date._isValid) {
                      setEditTask({...editTask, start: date})
                      setDateError(false)
                    }
                    else
                      setDateError(true)
                  }}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item xs={12} md={4}>
              <MuiPickersUtilsProvider utils={MomentUtils}>
                <KeyboardDatePicker
                  className={classes.form}
                  margin="normal"
                  name="end"
                  fullWidth
                  label="End Date"
                  format="DD/MM/yyyy"
                  value={editTask.end}
                  error={dateError || editTask.start >= editTask.end}
                  helperText={dateError || editTask.start >= editTask.end ? "Enter valid date!" : ""}
                  onChange={(date) => {
                    if(date._isValid) {
                      setEditTask({...editTask, end: date})
                      setDateError(false)
                    }
                    else
                      setDateError(true)
                  }}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item xs={false} md={2}></Grid>

            {/* <Grid item xs={0} md={2}></Grid>
            <Grid item xs={12} md={10}>
              <FormGroup row>
                <FormControlLabel
                  control={<Checkbox checked={addTask.allDay} name="allDay" onChange={() => setAddTask({...addTask, allDay: !addTask.allDay})} />}
                  label="All Day Task"
                />
              </FormGroup>
            </Grid> */}

            <Grid item xs={12} md={2}></Grid>
            <Grid item xs={10} md={4}>
              <FormControl fullWidth variant="outlined" className={classes.form}>
                <InputLabel id="progress-select-label">Status</InputLabel>
                <Select
                  name="status"
                  labelId="progress-select-label"
                  id="progress-select"
                  value={editTask.status}
                  onChange={editTaskHandle}
                  label="Status"
                  InputLabelProps={{shrink:true}} //Modified line
                >
                  <MenuItem className={classes.grey} value="To-Do">To-Do</MenuItem>
                  <MenuItem className={classes.blue} value="In-Progress">In-Progress</MenuItem>
                  <MenuItem className={classes.green} value="Completed">Completed</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={2} md={4}>
              <Tooltip title={editTask.status === 'Completed'?"Task Completed!":"Next Status"} placement="right" arrow>
                <span>
                  <IconButton disabled={editTask.status === 'Completed'} className={classes.form} onClick={nextStatusEdit}>
                    <ArrowRightIcon />
                  </IconButton>
                </span>
              </Tooltip>
            </Grid>
            <Grid item xs={12} md={2}></Grid>

            <Grid item xs={false} md={2}></Grid>
            <Grid item xs={12} md={4}>
              <Button
                className={classes.button}
                onClick={deleteTask}
                color="secondary"
                variant="contained"
              >
                DELETE
              </Button>
            </Grid>
            <Grid item xs={12} md={4}>
              <Button
                className={classes.button}
                onClick={editTaskSubmit}
                color="primary"
                variant="contained"
                disabled={editTask.title === "" || dateError || editTask.start >= editTask.end}
              >
                EDIT
              </Button>
            </Grid>
            <Grid item xs={false  } md={2}></Grid>
            
          </Grid>
        </DialogContent>
      </Dialog>

      {/* alert popups */}
      <Snackbar
				open={successAddTask}
				onClose={() => setSuccessAddTask(false)}
			>
				<Alert variant="filled" severity="success">
					Task added succesfully!
			  </Alert>
			</Snackbar>

      <Snackbar
				open={successEditTask}
				onClose={() => setSuccessEditTask(false)}
			>
				<Alert variant="filled" severity="success">
					Task edit succesful!
			  </Alert>
			</Snackbar>

      <Snackbar
				open={successEditTaskStatus}
				onClose={() => setSuccessEditTaskStatus(false)}
			>
				<Alert variant="filled" severity="success">
					Status edit succesful!
			  </Alert>
			</Snackbar>

      <Snackbar
				open={successDeleteTask}
				onClose={() => setSuccessDeleteTask(false)}
			>
				<Alert variant="filled" severity="error">
					Task deleted!
			  </Alert>
			</Snackbar>
    </div>
  )
}

export default Calendar;

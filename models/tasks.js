const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const taskSchema = new Schema({
  allDay: {
    type: Boolean,
    default: true
  },
  title: {
    type: String,
    required: true
  },
  start: {
    type: Date,
  },
  end: {
    type: Date,
  },
  status: {
    type: String,
    default: 'To-Do'
  }
});

var Tasks = mongoose.model('Task', taskSchema);

module.exports = Tasks;
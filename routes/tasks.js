var express = require('express');
var tasksRouter = express.Router();

var Tasks = require('../models/tasks');

/* GET home page. */
tasksRouter.route('/')
.get(async function(req, res, next) {
  try {
    var result = await Tasks.find({});
    res.statusCode=200;
    res.setHeader('Content-Type', 'application/json');
    res.json(result);
  } catch (error) {
    console.log(error);
  } 
})
.post(async function(req,res,next) {
  try {
    var result= await Tasks.create(req.body);
    res.statusCode=200;
    res.setHeader('Content-Type', 'application/json');
    res.json(result);
  } catch (error) {
    console.log(error);
  }
});

tasksRouter.route('/:taskId')
.get(async function(req, res, next) {
  try {
    var result = await Tasks.findById(req.params.taskId);
    res.statusCode=200;
    res.setHeader('Content-Type', 'application/json');
    res.json(result);
  } catch (error) {
    console.log(error); 
  }
})
.put(async function(req,res,next) {
  try {
    var result = await Tasks.findByIdAndUpdate(req.params.taskId, {
      $set: req.body
    }, { new: true });
    res.statusCode=200;
    res.setHeader('Content-Type', 'application/json');
    res.json(result);
  } catch (error) {
    console.log(error); 
  }
})
.delete(async function(req,res,next) {
  try {
    var result = await Tasks.findByIdAndRemove(req.params.taskId);
    res.statusCode=200;
    res.setHeader('Content-Type', 'application/json');
    res.json(result);
  } catch (error) {
    console.log(error); 
  }
});

module.exports = tasksRouter;